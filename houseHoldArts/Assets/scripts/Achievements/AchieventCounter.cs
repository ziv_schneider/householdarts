﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

 
struct AchieventCounter
{
    public int total;
    public int achieved;
}