1
00:00:00,000 --> 00:00:04,500
Do not invest in many ornaments. A good

2
00:00:02,310 --> 00:00:07,049
thing to remember is that ornaments

3
00:00:04,500 --> 00:00:09,690
decrease in value as they increase in number.

4
00:00:07,049 --> 00:00:11,670
A few bits of color pottery or

5
00:00:09,690 --> 00:00:14,790
some brassware is all that is required

6
00:00:11,670 --> 00:00:16,500
to strike a lively note. Place these so

7
00:00:14,790 --> 00:00:18,539
that they will balance other objects

8
00:00:16,500 --> 00:00:21,210
arranged on the same mantle or bookshelf.

9
00:00:18,539 --> 00:00:24,090
For example a pair of brass candlesticks

10
00:00:21,210 --> 00:00:26,670
placed at either end of a mantel with a

11
00:00:24,090 --> 00:00:30,020
pottery bowl clock or ornament in the

12
00:00:26,670 --> 00:00:32,399
middle, this strikes a balance


