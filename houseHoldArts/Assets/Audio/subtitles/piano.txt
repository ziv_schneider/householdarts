1
00:00:00,920 --> 00:00:05,100
the floor plans for this house 

2
00:00:03,449 --> 00:00:08,309
were replicated from an existing colonial house

3
00:00:05,100 --> 00:00:09,750
built in 1750. It was allegedly the birthplace

4
00:00:08,309 --> 00:00:12,179
of the actor and playwright

5
00:00:09,750 --> 00:00:13,769
John Howard Payne and they chose it

6
00:00:12,179 --> 00:00:15,540
because they thought it was the house he wrote

7
00:00:13,769 --> 00:00:17,550
the lyrics to his famous song "Home Sweet Home" about,

8
00:00:15,540 --> 00:00:19,439
the song I'm playing for you right now!

9
00:00:17,550 --> 00:00:25,700
this theory has recently been proven wrong

10
00:00:19,439 --> 00:00:25,050


